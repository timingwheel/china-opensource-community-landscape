# MongoDB中文社区

## （一）社区简介

MongoDB 中文社区（mongoing.com)成立于2014年，是大中华区获得官方认可的中文社区，致力于做一个精而美的 MongoDB 技术交流平台。中文社区的愿景是:为广大 MongoDB 中文爱好者创建一个活跃的互助平台;推广 MongoDB 成为企业数据库应用的首选方案;聚集 MongoDB 开发、数据库、运维专家，打造最权威的技术社区。

![输入图片说明](logo/mongodb.png)

## （二）治理模式

为更好地服务国内 MongoDB 的⽤户及众多⽀持 MongoDB 发展的企业，社区在主要核⼼成员中设常委会委员、内容战略主席、地方分会主席、主席团成员。常委席位设置5位，其中1位任轮值主席，常委和主席负责社区建设监管、重⼤事件决策、各类事务协调以及整体⽅向的把控；内容战略主席设有3位，负责各自具体⼯作⽅向的事务管理包括内容、培训、翻译；地方分会主席设有8位，分别设立在北京、上海、广州、深圳、杭州、长沙、西安、南京、成都，负责对地方成员及活动输出的把控和出品。

除以上席位，社区还常设核心成员，负责社区内用户问题的解决，技术干货、文档翻译、技术问答、活动议题的输出，为社区提供全方位的支撑。

## （三）运营实践

经过社区志愿者们的不断努力，目前已经有超过5万+的线上及线下成员。中文社区由技术大会、技术培训、内容全视角、技术问答等版块组成，迄今为止已经成功举办了10+场线下活动和线上直播，发表了100+篇优质技术文章，相关合作单位已达20+家，微信公众号人数突破1w+，微信社群人数突破3000+，社区自媒体矩阵覆盖量超过1w+。

![输入图片说明](images/mongodb-1.png)

![输入图片说明](images/mongodb-2.png)

![输入图片说明](images/mongodb-3.png)